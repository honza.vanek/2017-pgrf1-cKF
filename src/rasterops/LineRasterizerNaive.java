package rasterops;

import rasterdata.RasterImage;

public class LineRasterizerNaive<PixelType> implements LineRasterizer<PixelType> {

    @Override
    public RasterImage<PixelType> rasterizeLine(
            final RasterImage<PixelType> img,
            final double x1, final double y1, final double x2, final double y2,
            PixelType value) {
        final double rx1 = x1 * (img.getWidth() - 1);
        final double ry1 = (1 - y1) * (img.getHeight() - 1);
        final double rx2 = x2 * (img.getWidth() - 1);
        final double ry2 = (1 - y2) * (img.getHeight() - 1);
        final double k = (ry2 - ry1) / (rx2 - rx1);
        final double q = ry1 - k * rx1;
        RasterImage<PixelType> result = img;
        for (int c = (int) rx1; c <= rx2; c++) {
            final double ry = k * c + q;
            result = result.withPixel(c, (int) ry, value);
        }
        return result;
    }
}
